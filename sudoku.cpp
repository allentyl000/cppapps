#include <iostream>
#include <string>
#include <vector>
#include <cmath>


using namespace std;
int n;
int b;
bool err = false;
vector<vector<int> > grid;
namespace getVars {
    int main(){
        bool inst = false;
        string wantInst;
        cout << "do you want instructions?[yes/no] ";
        getline(cin, wantInst);
        if(wantInst == "yes"){
            inst = true;
        }
        if(wantInst == "no"){
            inst = false;
        }
        if(inst){
        cout << "put number of rows: ";
        cin >> n;
            try {
                b = sqrt(n);
            } catch(exception e) {
                cout << e.what();
            }
            cout << "now, put in each row of the puzzle with zeros for blanks\n";
        }
        if(!inst){
        cout << "put number of rows: ";
        cin >> n;
            try {
                b = sqrt(n);
            } catch(exception e) {
                cout << e.what();
            }
            
        }
        
        
        #define N n
        #define B b
        
        int line;
        bool errs = false;
        string no;
        grid.resize(N, vector<int>(N));
        
        cout << "line 1: ";
        int count = 0;
        
        while (count < N && getline(cin, no)) {
            
            err = false;
             if(no == "8108"){
            err = true;
cout << "                                 ._                                 " << endl;
cout << "                              ,-\'_ `-.                              " << endl;
cout << "                              ::\".^-. `.                            " << endl;
cout << "                              ||<    >. \\                           " << endl;
cout << "                              |: _, _| \\ \\                          " << endl;
cout << "                              : .\'| \'|  ;\\`.                        " << endl;
cout << "                              _\\ .`  \'  | \\ \\                       " << endl;
cout << "                            .\' `\\ *-\'   ;  . \\                      " << endl;
cout << "                           \'\\ `. `.    /\\   . \\                     " << endl;
cout << "                         _/  `. \\  \\  :  `.  `.;                    " << endl;
cout << "                       _/ \\  \\ `-._  /|  `  ._/                     " << endl;
cout << "                      / `. `. `.   /  :    ) \\                      " << endl;
cout << "                      `;._.  \\  _.\'/   \\ .\' .\';                     " << endl;
cout << "                      /     .\'`._.* /    .-\' (                      " << endl;
cout << "                    .\'`._  /    ; .\' .-\'     ;                      " << endl;
cout << "                    ; `._.:     |(    ._   _.\'|                     " << endl;
cout << "                    `._   ;     ; `.-\'        |                     " << endl;
cout << "                     |   / .-\'./ .\'  \\ .     /:                     " << endl;
cout << "                     |  +.\'  \\ `-.   .\\ *--*\' ;\\                    " << endl;
cout << "                     ;.\' `. \\ `.    /` `.    /  .                   " << endl;
cout << "                    /.L-\'\\_: L__..-*     \\   \".  \\                  " << endl;
cout << "                   :/ / .\' `\' ;   `-.     `.   \\  .                 " << endl;
cout << "                   / /_/     /              \\   ;  \\                " << endl;
cout << "              |  _/ /       /          \\     `./    .               " << endl;
cout << "            `   .  ;       /    .'      `-.   ;      \\              " << endl;
cout << "           --  /  /  --   ,    /           `\"\' \\      .             " << endl;
cout << "          .   .  \'       /   .\'                 `.     \\            " << endl;
cout << "             /  /    `  /   /                  |  `-.   .           " << endl;
cout << "        --  .  \'   \\   /                         `.  `-._\\          " << endl;
cout << "       .   /  /       : `*.                    :   `.    `-.        " << endl;
cout << "          .  \'    `   |    \\                    \\    `-._   `-._    " << endl;
cout << "     --  /  /   \\     :     ;                    \\              |   " << endl;
cout << "   .    .  \'           ;                         `.  \\      :  ;   " << endl;
cout << "       /  /   `       : \\    \\                      `. `._  /  /    " << endl;
cout << "  --  .  \'  \\         |  `.   `.                      `-. `\'  /\\    " << endl;
cout << "     /  .             ;         `-.              \\       `-..\'  ;   " << endl;
cout << " `  .  \'   `          |__                     |   `.         `-._.  " << endl;
cout << "_  :  /  \\              ;`-.                  :     `-.           ; " << endl;
cout << "    `\"  `               |   `.                 \\       `*-.__.-*\"\' \\" << endl;
cout << "\' /  . \\                ;_.  :`-._              `._                /" << endl;
cout << "                       /   `  . ; `\"*-._                       _.-` " << endl;
cout << "                     .\'\"\'    _;  `-.__                     _.-`     " << endl;
cout << "                     `-.__.-\"         `\"\"---...___...--**\"\' |       " << endl;
cout << "                                                  `.____..--\'" << endl;
        }
        try {
        if(no.length() == N){
            
            line = stoi(no);
            
            
	    for (int i = N-1; i >= 0; i--) {
            grid[count][i] = line % 10;
            line /= 10;
        }
        if(count < N-1 && err == false){
        
        cout << "line " << count+2 << ": ";
        
        } 
            count += 1;
        }
        if(err == true){
            count -= 1;
            cout << "line " << count+1 << ": ";
        }
            } catch(exception ex){
        cout << ex.what();
    }
        
    } 
        
    }
}
int count = 0;

bool isPresentInCol(int col, int num){ //check whether num is present in col or not
   for (int row = 0; row < N; row++)
      if (grid[row][col] == num)
         return true;
   return false;
}
bool isPresentInRow(int row, int num){ //check whether num is present in row or not
   for (int col = 0; col < N; col++)
      if (grid[row][col] == num)
         return true;
   return false;
}
bool isPresentInBox(int boxStartRow, int boxStartCol, int num){
//check whether num is present in 3x3 box or not
   for (int row = 0; row < B; row++)
      for (int col = 0; col < B; col++)
         if (grid[row+boxStartRow][col+boxStartCol] == num)
            return true;
   return false;
}
void sudokuGrid(){ //print the sudoku grid after solve
   for (int row = 0; row < N; row++){
      for (int col = 0; col < N; col++){
          for(int s = 1; s < B; s++){
         if(col == B*s)
            cout << " | ";
          }
         cout << grid[row][col] <<" ";
      
      }
      for(int u = 1; u < B; u++){
      if(row == (B*u)-1){
         cout << endl;
         for(int i = 0; i<N; i++)
            cout << "---";
      }
      }
      cout << endl;
   }
}
bool findEmptyPlace(int &row, int &col){ //get empty location and update row and column
   for (row = 0; row < N; row++)
      for (col = 0; col < N; col++)
         if (grid[row][col] == 0) //marked with 0 is empty
            return true;
   return false;
}
bool isValidPlace(int row, int col, int num){
   //when item not found in col, row and current 3x3 box
   return !isPresentInRow(row, num) && !isPresentInCol(col, num) && !isPresentInBox(row - row%B ,
col - col%B, num);
}
bool solveSudoku(){
   int row, col;
   if (!findEmptyPlace(row, col))
      return true; //when all places are filled
   for (int num = 1; num <= N; num++){ //valid numbers are 1 - 9
      if (isValidPlace(row, col, num)){ //check validation, if yes, put the number in the grid
         grid[row][col] = num;
         if (solveSudoku()) //recursively go for other rooms in the grid
            return true;
         grid[row][col] = 0; //turn to unassigned space when conditions are not satisfied
      }
   }
   return false;
}
int main(){
    getVars::main();
    
    
    
    if (solveSudoku() == true)
      sudokuGrid();
   else
      cout << "No solution exists";
      return 0;
   
}
